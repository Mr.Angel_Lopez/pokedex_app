package com.monsh.pokeapp.models;

/**
 * Created by monsh on 02/05/2017.
 */

public class Pokemon {

    private int number;
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public int getNumber() {
        String[] urlPartes = url.split("/");
        return Integer.parseInt(urlPartes[urlPartes.length - 1]);
    }
    public int setNumber(int number) {
        String[] UrlPartes = url.split("/");
        return Integer.parseInt(UrlPartes[UrlPartes.length - 1]);
    }

}